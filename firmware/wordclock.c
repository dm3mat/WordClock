#include "wordclock.h"
#include "display.h"
#include "ds1307.h"

#include <avr/io.h>
#include <avr/interrupt.h>


volatile uint8_t _wordclock_needs_update = 0;
static datetime_t _wordclock_now;

void
wordclock_init(void) {
  display_init();
  ds1307_init();
  usb_init();

  _wordclock_needs_update = 1;

  // Init timer 3 -> 1s tick
  TCCR3A = (0<<COM3A1) | (0<<COM3A0)
      | (0<<COM3B1) | (0<<COM3B0)
      | (0<<COM3C1) | (0<<COM3C0)
      | (0<<WGM31)  | (0<<WGM30);
  TCCR3B = (0<<ICNC3) | (0<<ICES3)
      | (0<<WGM33) | (1<<WGM32)
      | (1<<CS32) | (0<<CS31) | (1<<CS30);
  OCR3A = 15625-1;
  TIMSK3 |= (1<<OCIE3A);
}

void
wordclock_task(void) {
  // If an update is needed
  if (! _wordclock_needs_update)
    return;

  // Get current datetime
  ds1307_get(&_wordclock_now);
}


ISR(TIMER3_COMPA_vect) {
  // update every second
  _wordclock_needs_update = 1;
}
