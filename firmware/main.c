#include "display.h"
#include "wordclock.h"
#include <avr/interrupt.h>


int main(void) {
  wordclock_init();

  sei();

  // enable only, once interrupts are enabled.
  display_enable();

  while(1) {
    usb_cdc_task();
    wordclock_task();
  }

  return 0;
}
