#ifndef __I2C_H__
#define __I2C_H__

#include <inttypes.h>

void i2c_init(void);

void i2c_start(void);
void i2c_address(uint8_t addr, uint8_t w);
void i2c_send(uint8_t data);
int8_t i2c_recv(void);
int8_t i2c_recv_ack(void);
void i2c_stop(void);


#endif // __I2C_H__
