#include "i2c.h"
#include <avr/io.h>

#define SCL_CLOCK 100000UL

void
i2c_init(void) {
  TWBR = ((( F_CPU / SCL_CLOCK ) - 16) / 2);
  TWSR = 0;
  TWCR = ( 1 << TWEN ); // enable the i2c bus function
  PORTD |= ((1 << PB1) | (1 << PB0)); // wnable pull-ups
}

void
i2c_start(void) {
  // send START condition
  TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);
  // wait until transmission completed
  while ( !(TWCR & (1<<TWINT)))	;
}

void
i2c_address(uint8_t addr, uint8_t w) {
  // send START condition
  TWDR = ((addr&0x7f)<<1) | (w&0x01) ;
  TWCR = (1 << TWINT) | (1<<TWEN);
  // wait until transmission completed
  while(!(TWCR & (1 << TWINT)));
}

void
i2c_stop(void) {
  TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);
  while(TWCR & (1 << TWSTO));
}

void
i2c_send(uint8_t data) {
  // send data to the previously addressed device
  TWDR = data;
  TWCR = (1 << TWINT) | (1<<TWEN);
  // wait until transmission completed
  while(!(TWCR & (1 << TWINT)));
}

int8_t
i2c_recv(void) {
  TWCR = (1 << TWINT) | (1 << TWEN);
  while(!(TWCR & (1 << TWINT)));
  return TWDR;
}

int8_t
i2c_recv_ack(void) {
  TWCR = (1 << TWINT) | (1 << TWEN) | (1<<TWEA);
  while(!(TWCR & (1 << TWINT)));
  return TWDR;
}
