#ifndef USB_H
#define USB_H

#include "descriptors.h"

#include <LUFA/Drivers/USB/USB.h>
#include <LUFA/Drivers/Board/Joystick.h>
#include <LUFA/Drivers/Board/LEDs.h>
#include <LUFA/Platform/Platform.h>

void usb_init(void);
void usb_cdc_task(void);

void event_usb_device_connect(void);
void event_usb_device_disconnect(void);
void event_usb_device_configuration_changed(void);
void event_usb_device_control_request(void);

#endif // USB_H
