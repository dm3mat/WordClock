#include "ds1307.h"
#include "i2c.h"


#define DS1307_DEV_ADDR 0x68
#define DS1307_SEC_REG  0x00
#define DS1307_MIN_REG  0x01
#define DS1307_HOR_REG  0x02
#define DS1307_DOW_REG  0x03
#define DS1307_DAY_REG  0x04
#define DS1307_MON_REG  0x05
#define DS1307_YR_REG   0x06
#define DS1307_CTRL_REG 0x07

void
ds1307_init(void) {
  i2c_init();

  i2c_start();
  i2c_address(DS1307_DEV_ADDR, 1);
  i2c_send(DS1307_CTRL_REG);
  i2c_send(0x00); // disable square wave output
}

void
ds1307_get(datetime_t *dt) {
  uint8_t byte = 0;

  i2c_start();
  i2c_address(DS1307_DEV_ADDR, 0);
  i2c_send(DS1307_SEC_REG);

  byte = i2c_recv_ack(); dt->seconds   = BCD2DEC(byte&0x7f);
  byte = i2c_recv_ack(); dt->minutes   = BCD2DEC(byte&0x7f);
  byte = i2c_recv_ack(); dt->hours     = BCD2DEC(byte&0x3f);
  byte = i2c_recv_ack(); dt->dayofweek = byte&0x07;
  byte = i2c_recv_ack(); dt->day       = BCD2DEC(byte&0x3f);
  byte = i2c_recv_ack(); dt->month     = BCD2DEC(byte&0x1f);
  byte = i2c_recv();     dt->year      = BCD2DEC(byte) + 2000;

  i2c_stop();
}

void
ds1307_set(const datetime_t *dt) {
  uint8_t byte;

  i2c_start();
  i2c_address(DS1307_DEV_ADDR, 1);
  i2c_send(DS1307_SEC_REG);

  byte = DEC2BCD(dt->seconds) & 0x7f; i2c_send(byte);
  byte = DEC2BCD(dt->minutes) & 0x7f; i2c_send(byte);
  byte = DEC2BCD(dt->hours) & 0x3f;   i2c_send(byte);
  byte = dt->dayofweek & 0x07;        i2c_send(byte);
  byte = DEC2BCD(dt->day) & 0x3f;     i2c_send(byte);
  byte = DEC2BCD(dt->month) & 0x1f;   i2c_send(byte);
  byte = DEC2BCD(dt->year%100);       i2c_send(byte);

  i2c_stop();
}
