#ifndef __WORDCLOCK_H__
#define __WORDCLOCK_H__

void wordclock_init(void);

void wordclock_poll(void);

#endif
