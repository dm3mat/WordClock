#ifndef __DS1307_H__
#define __DS1307_H__

#include <inttypes.h>

#define BCD2DEC(a) ((a&0xf) + (10*(a>>4)))
#define DEC2BCD(a) ((a%10) + ((a/10)<<4))

typedef struct {
  uint8_t seconds;
  uint8_t minutes;
  uint8_t hours;
  uint8_t dayofweek;
  uint8_t day;
  uint8_t month;
  uint8_t year;
} datetime_t;

void ds1307_init(void);
void ds1307_set(const datetime_t *dt);
void ds1307_get(datetime_t *dt);



#endif // __DS1307_H__
