#ifndef __DISPLAY_H__
#define __DISPLAY_H__


/** Initializes the display column and row interface, as well as the timer, controlling the 100Hz
 * output. */
void display_init(void);

/** Blanks the display, also stops the timer. */
void display_blank(void);
/** Enables the display, also restarts the timer. */
void display_enable(void);


#endif //__DISPLAY_H__
