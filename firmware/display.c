#include "display.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>

#define A0_DDR     DDRF
#define A0_PORT    PORTF
#define A0_BIT     PF0
#define A1_DDR     DDRF
#define A1_PORT    PORTF
#define A1_BIT     PF1
#define A2_DDR     DDRF
#define A2_PORT    PORTF
#define A2_BIT     PF4
#define A3_DDR     DDRF
#define A3_PORT    PORTF
#define A3_BIT     PF5

#define K0_DDR     DDRD
#define K0_PORT    PORTD
#define K0_BIT     PD2
#define K1_DDR     DDRD
#define K1_PORT    PORTD
#define K1_BIT     PD3
#define K2_DDR     DDRD
#define K2_PORT    PORTD
#define K2_BIT     PD5
#define K3_DDR     DDRD
#define K3_PORT    PORTD
#define K3_BIT     PD4
#define K4_DDR     DDRD
#define K4_PORT    PORTD
#define K4_BIT     PD6
#define K5_DDR     DDRD
#define K5_PORT    PORTD
#define K5_BIT     PD7
#define K6_DDR     DDRB
#define K6_PORT    PORTB
#define K6_BIT     PB4
#define K7_DDR     DDRD
#define K7_PORT    PORTD
#define K7_BIT     PB5

#define nBLANK_DDR  DDRB
#define nBLANK_PORT PORTB
#define nBLANK_BIT  PB6

volatile uint8_t _display_column_address = 0;
volatile uint8_t _display_buffer[16] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};


void
display_init(void) {
  // Configure blanking pin
  nBLANK_DDR |= (1<<nBLANK_BIT); nBLANK_PORT |= (1<<nBLANK_BIT);

  // Configure kathode driver pins
  K0_DDR |= (1<<K0_BIT); K0_PORT &= ~(1<<K0_BIT);
  K1_DDR |= (1<<K1_BIT); K1_PORT &= ~(1<<K1_BIT);
  K2_DDR |= (1<<K2_BIT); K2_PORT &= ~(1<<K2_BIT);
  K3_DDR |= (1<<K3_BIT); K3_PORT &= ~(1<<K3_BIT);
  K4_DDR |= (1<<K4_BIT); K4_PORT &= ~(1<<K4_BIT);
  K5_DDR |= (1<<K5_BIT); K5_PORT &= ~(1<<K5_BIT);
  K6_DDR |= (1<<K6_BIT); K6_PORT &= ~(1<<K6_BIT);
  K7_DDR |= (1<<K7_BIT); K7_PORT &= ~(1<<K7_BIT);

  // Configure column address ports
  A0_DDR |= (1<<A0_BIT); A0_PORT &= ~(1<<A0_BIT);
  A1_DDR |= (1<<A1_BIT); A1_PORT &= ~(1<<A1_BIT);
  A2_DDR |= (1<<A2_BIT); A2_PORT &= ~(1<<A2_BIT);
  A3_DDR |= (1<<A3_BIT); A3_PORT &= ~(1<<A3_BIT);
  _display_column_address = 0;

  // Configure timer 1
  TCCR1A = (0<<COM1A1) | (0<<COM1A0)
      | (0<<COM1B1) | (0<<COM1B0)
      | (0<<COM1C1) | (0<<COM1C0)
      | (0<<WGM11)  | (0<<WGM10);
  TCCR1B = (0<<ICNC1) | (0<<ICES1)
      | (0<<WGM13) | (1<<WGM12)
      | (0<<CS12) | (0<<CS11) | (0<<CS10);
  OCR1A = 1250-1;
  TIMSK1 &= ~(1<<OCIE1A);
}

inline void
display_blank(void) {
  nBLANK_PORT |= (1<<nBLANK_BIT);
  // Stop timer & disable interrupt
  TCCR1B &= ~((1<<CS12) | (1<<CS11) | (1<<CS10));
  TIMSK1 &= ~(1<<OCIE1A);
}

inline void
display_enable(void) {
  nBLANK_PORT &= ~(1<<nBLANK_BIT);
  // Enable timer & enable interrupt
  TCCR1B |= (0<<CS12) | (1<<CS11) | (0<<CS10);
  TIMSK1 |= (1<<OCIE1A);
  _display_column_address = 0;
}

inline void
_display_next_column(void) {
  // Increment column address
  _display_column_address++; _display_column_address &= 0x0f;
  uint8_t addr = _display_column_address;
  // Set column address
  if (addr & 1) A0_PORT |= (1<<A0_BIT); else A0_PORT &= ~(1<<A0_BIT); addr >>= 1;
  if (addr & 1) A1_PORT |= (1<<A1_BIT); else A1_PORT &= ~(1<<A1_BIT); addr >>= 1;
  if (addr & 1) A2_PORT |= (1<<A2_BIT); else A2_PORT &= ~(1<<A2_BIT); addr >>= 1;
  if (addr & 1) A3_PORT |= (1<<A3_BIT); else A3_PORT &= ~(1<<A3_BIT); addr >>= 1;
  // Read column
  uint8_t col = _display_buffer[_display_column_address];
  // Set column
  if (col & 1) K0_PORT |= (1<<K0_BIT); else K0_PORT &= ~(1<<K0_BIT); col >>= 1;
  if (col & 1) K1_PORT |= (1<<K1_BIT); else K1_PORT &= ~(1<<K1_BIT); col >>= 1;
  if (col & 1) K2_PORT |= (1<<K2_BIT); else K2_PORT &= ~(1<<K2_BIT); col >>= 1;
  if (col & 1) K3_PORT |= (1<<K3_BIT); else K3_PORT &= ~(1<<K3_BIT); col >>= 1;
  if (col & 1) K4_PORT |= (1<<K4_BIT); else K4_PORT &= ~(1<<K4_BIT); col >>= 1;
  if (col & 1) K5_PORT |= (1<<K5_BIT); else K5_PORT &= ~(1<<K5_BIT); col >>= 1;
  if (col & 1) K6_PORT |= (1<<K6_BIT); else K6_PORT &= ~(1<<K6_BIT); col >>= 1;
  if (col & 1) K7_PORT |= (1<<K7_BIT); else K7_PORT &= ~(1<<K7_BIT); col >>= 1;
}


ISR(TIMER1_COMPA_vect) {
  _display_next_column();
}
