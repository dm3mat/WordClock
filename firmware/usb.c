#include "usb.h"

#include <stdbool.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#include <string.h>

static CDC_LineEncoding_t LineEncoding = {
  .BaudRateBPS = 0,
  .CharFormat  = CDC_LINEENCODING_OneStopBit,
  .ParityType  = CDC_PARITY_None,
  .DataBits    = 8
};

/** Configures the board hardware and chip peripherals for the demo's functionality. */
void
usb_init(void)
{
  /* Disable watchdog if enabled by bootloader/fuses */
  MCUSR &= ~(1 << WDRF);
  wdt_disable();

  /* Disable clock division */
  clock_prescale_set(clock_div_1);

  USB_Init();
}

/** Event handler for the USB_Connect event. This indicates that the device is enumerating via the status LEDs and
 *  starts the library USB task to begin the enumeration and USB management process. */
void
event_usb_device_connect(void)
{
  // pass...
}

/** Event handler for the USB_Disconnect event. This indicates that the device is no longer connected to a host via
 *  the status LEDs and stops the USB management and CDC management tasks. */
void
event_usb_device_disconnect(void)
{
  // pass...
}

/** Event handler for the USB_ConfigurationChanged event. This is fired when the host set the current configuration
 *  of the USB device after enumeration - the device endpoints are configured and the CDC management task started.
 */
void event_usb_device_configuration_changed(void)
{
  bool ConfigSuccess = true;

  /* Setup CDC Data Endpoints */
  ConfigSuccess &= Endpoint_ConfigureEndpoint(CDC_NOTIFICATION_EPADDR, EP_TYPE_INTERRUPT, CDC_NOTIFICATION_EPSIZE, 1);
  ConfigSuccess &= Endpoint_ConfigureEndpoint(CDC_TX_EPADDR, EP_TYPE_BULK, CDC_TXRX_EPSIZE, 1);
  ConfigSuccess &= Endpoint_ConfigureEndpoint(CDC_RX_EPADDR, EP_TYPE_BULK,  CDC_TXRX_EPSIZE, 1);

  /* Reset line encoding baud rate so that the host knows to send new values */
  LineEncoding.BaudRateBPS = 0;
}

/** Event handler for the USB_ControlRequest event. This is used to catch and process control requests sent to
 *  the device from the USB host before passing along unhandled control requests to the library for processing
 *  internally.
 */
void event_usb_device_control_request(void)
{
  /* Process CDC specific control requests */
  switch (USB_ControlRequest.bRequest)
  {
  case CDC_REQ_GetLineEncoding:
    if (USB_ControlRequest.bmRequestType == (REQDIR_DEVICETOHOST | REQTYPE_CLASS | REQREC_INTERFACE)) {
      Endpoint_ClearSETUP();

      /* Write the line coding data to the control endpoint */
      Endpoint_Write_Control_Stream_LE(&LineEncoding, sizeof(CDC_LineEncoding_t));
      Endpoint_ClearOUT();
    }
    break;

  case CDC_REQ_SetLineEncoding:
    if (USB_ControlRequest.bmRequestType == (REQDIR_HOSTTODEVICE | REQTYPE_CLASS | REQREC_INTERFACE)) {
      Endpoint_ClearSETUP();

      /* Read the line coding data in from the host into the global struct */
      Endpoint_Read_Control_Stream_LE(&LineEncoding, sizeof(CDC_LineEncoding_t));
      Endpoint_ClearIN();
    }
    break;

  case CDC_REQ_SetControlLineState:
      if (USB_ControlRequest.bmRequestType == (REQDIR_HOSTTODEVICE | REQTYPE_CLASS | REQREC_INTERFACE)) {
        Endpoint_ClearSETUP();
        Endpoint_ClearStatusStage();

        /* NOTE: Here you can read in the line state mask from the host, to get the current state of the output handshake
                 lines. The mask is read in from the wValue parameter in USB_ControlRequest, and can be masked against the
             CONTROL_LINE_OUT_* masks to determine the RTS and DTR line states using the following code:
        */
      }
      break;
  }
}

/** Function to manage CDC data transmission and reception to and from the host. */
void usb_cdc_task(void)
{
  /* Device must be connected and configured for the task to run */
  if (USB_DeviceState != DEVICE_STATE_Configured)
    return;

}
