spacing   = 8;
h_padding = 10; 
v_padding = 5;

module front(spacing = 8, h_padding = 10, v_padding = 5) {
  font = "BPmonoStencil:style=Bold";
  letter_size = spacing-3;

  width  = 16*spacing+2*h_padding;
  height = 8*spacing+2*v_padding;
  depth  = 3;

  text = ["ESBISTONFÜNFZEHN",
          "DUKURZWANZIGENAU",
          "DREIVIERTELONACH",
          "VORXHALBUMPZWÖLF",
          "ZWEINSIEBENEUNYJ",
          "ELFÜNFKDREISECHS",
          "ACHTZEHNVIERKUHR",
          "NACHTSVORMITTAGS"];

  difference() {
    cube([width, height, depth]);
    for (i=[0:7]) {
      for (j=[0:15]) {
        translate([h_padding+j*spacing, height-(i+1)*spacing-v_padding,-0.5])
          linear_extrude(depth+1)
            text(text[i][j], size = letter_size, font = font, 
                halign = "left", valign = "baseline", $fn = 64);
      }
    }
  }
}

module diffusor(spacing = 8, h_padding = 10, v_padding = 5) {
  width  = 16*spacing+2*h_padding;
  height = 8*spacing+2*v_padding;
  depth  = 3;
  
  for (i=[0:7]) {
    for (j=[0:15]) {
      translate([h_padding+spacing*j, height-(i+1)*spacing-v_padding,0])
        cube([spacing*0.7,spacing*.9,depth]);
    }
  }
}

module lightmask(spacing = 8, h_padding = 10, v_padding = 5) {
  width  = 16*spacing+2*h_padding;
  height = 8*spacing+2*v_padding;
  depth  = 3;

  difference() {
    cube([width,height,depth]);
    translate([0,0,-0.5]) 
      scale([1,1,2]) 
        diffusor(spacing, h_padding, v_padding);
  }
}


color("Wheat") front(spacing, h_padding, v_padding);
color("black") translate([0,0,-10]) lightmask(spacing, h_padding, v_padding);
//color("white") translate([0,0,-15]) diffusor(spacing, h_padding, v_padding);
color("green")translate([3,4,-20]) import("WordClockPCB.stl");